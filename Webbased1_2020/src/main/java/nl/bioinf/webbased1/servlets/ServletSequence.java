package nl.bioinf.webbased1.servlets;

import nl.bioinf.webbased1.config.WebConfig;
import nl.bioinf.webbased1.model.InputCheck;
import nl.bioinf.webbased1.model.PatDataBase;
import nl.bioinf.webbased1.model.PatternMatch;
import nl.bioinf.webbased1.model.Protein;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

@WebServlet(name = "ServletSequence", urlPatterns = "/ServletSequence", loadOnStartup = 1)
public class ServletSequence extends HttpServlet {
    private TemplateEngine templateEngine;
    private ArrayList DB;
    private LinkedList<ArrayList<Protein>> HIST;
    private LinkedList<String> HIST_SEQ;

    @Override
    public void init() throws ServletException {
        System.out.println("Initializing Thymeleaf template engine");
        final ServletContext servletContext = this.getServletContext();
        WebConfig.createTemplateEngine(servletContext);
        this.templateEngine = WebConfig.getTemplateEngine();

    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        // Create Database
        System.out.println("INITIALISING DATABASE");
        super.init(config);
        String path = config.getServletContext().getInitParameter("path_prosite_data");
        setDB(PatDataBase.build(path));

        // Create History
        System.out.println("INITIALISING HISTORY");
        LinkedList<ArrayList<Protein>> p_history = new LinkedList<>();
        setHIST(p_history);

        LinkedList<String> s_history = new LinkedList<>();
        setHIST_SEQ(s_history);
    }

    private void setDB(ArrayList DB) {
        this.DB = DB;
    }

    private void setHIST(LinkedList<ArrayList<Protein>> HIST) {
        this.HIST = HIST;
    }

    public void setHIST_SEQ(LinkedList<String> HIST_SEQ) {
        this.HIST_SEQ = HIST_SEQ;
    }

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebConfig.configureResponse(response);
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());

        // Get Input String as UpperCase
        String input_seq = request.getParameter("sequence").toUpperCase();
        ArrayList matches = PatternMatch.patternMatcher(this.DB, input_seq);

        if (HIST.size() > 5) {
            HIST.removeFirst();
            HIST_SEQ.removeFirst();
        }

        if (InputCheck.checker(input_seq)) {
            //System.out.println("INPUT PASSED");
            HIST.add(matches);
            HIST_SEQ.add(input_seq);

            ctx.setVariable("input", input_seq);
            ctx.setVariable("proteins", matches);
            ctx.setVariable("history", HIST);
            ctx.setVariable("history_seq", HIST_SEQ);
            WebConfig.getTemplateEngine().process("output", ctx, response.getWriter());
        } else {
            System.out.println("INPUT REJECTED");
            WebConfig.getTemplateEngine().process("rejected", ctx, response.getWriter());
        }
    }
}
