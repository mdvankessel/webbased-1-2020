package nl.bioinf.webbased1.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class PatDataBase {
    public static ArrayList build(String file_path) {
        return readFile(file_path);
    }

    private static ArrayList readFile(String file_path) {
        Path path = Paths.get(file_path);
        ArrayList<String> id_list = new ArrayList<>();
        ArrayList<String> ac_list = new ArrayList<>();
        ArrayList<String> dt_list = new ArrayList<>();
        ArrayList<String> pa_list = new ArrayList<>();
        ArrayList<String> de_list = new ArrayList<>();

        HashMap<String, ArrayList<String>> fileVars = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(path)) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("ID")) {
                    id_list.add(line.split("   ")[1]);
                }

                if (line.startsWith("AC")) {
                    ac_list.add(line.split("   ")[1]);
                    if (id_list.size() > ac_list.size()) {
                        ac_list.add("$EMPTY$");
                    }
                }

                if (line.startsWith("DT")) {
                    dt_list.add(line.split("   ")[1]);
                    if (id_list.size() > dt_list.size()) {
                        dt_list.add("$EMPTY$");
                    }
                }

                if (line.startsWith("PA")) {
                    String pattern = patternChecker(line);
                    try {
                        pa_list.add(pattern.split("   ")[1]);
                        if (id_list.size() > pa_list.size()) {
                            pa_list.add("$EMPTY$");
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }

                if (line.startsWith("DE")) {
                    de_list.add(line.split("   ")[1]);
                    if (id_list.size() > de_list.size()) {
                        de_list.add("$EMPTY$");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<Protein> proteins = new ArrayList<>();
        for (int i = 1; i < pa_list.size(); i++) {
            if (pa_list.get(i) != null) {
                Protein protein = new Protein(id_list.get(i).split(";")[0], ac_list.get(i).split(";")[0], dt_list.get(i).split(";")[2], pa_list.get(i), de_list.get(i));
                proteins.add(protein);
            }
        }
        return proteins;
    }
    private static String patternChecker(String pattern) {
        pattern = pattern.replace("-", "");
        pattern = pattern.replace(".", "");
        pattern = pattern.replace(">", "");
        pattern = pattern.replace("x", "[A-Z]");
        pattern = pattern.replace("X", "[A-Z]");
        pattern = pattern.replace("(", "{");
        pattern = pattern.replace(")", "}");
        if (!pattern.equals("$EMPTY$")) {
            if (pattern.matches(".*\\{[A-Z]+\\}.*")) {
                pattern = "$EMPTY$";
            }
        }
        return pattern;
    }
}
