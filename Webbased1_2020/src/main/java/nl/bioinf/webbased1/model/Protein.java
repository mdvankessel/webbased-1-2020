package nl.bioinf.webbased1.model;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Protein {
    // id ac dt pa de
    private final String id;
    private final String ac;
    private final String dt;
    private final String pa;
    private final String de;

    public Protein(String id, String ac, String dt, String pa, String de) {
        this.id = id;
        this.ac = ac;
        this.dt = dt;
        this.pa = pa;
        this.de = de;
    }

    public String getId() {
        return id;
    }

    public String getAc() {
        return ac;
    }

    public String getDt() {
        return dt;
    }

    public String getPa() {
        return pa;
    }

    public String getDe() {
        return de;
    }
}
