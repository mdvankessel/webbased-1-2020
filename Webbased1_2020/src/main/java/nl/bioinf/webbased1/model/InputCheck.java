package nl.bioinf.webbased1.model;

public class InputCheck {
    public static boolean checker(String input) {
        input = whiteSpaceRemover(input);
        return inputChecker(input);
    }

    public static String whiteSpaceRemover(String input) {
        input = input.replace("\\n", "");
        input = input.replace(" ", "");
        input = input.replace("\\t", "");
        return input;
    }
    public static boolean inputChecker(String input) {
        return input.matches("[A-Z]+");
    }
}
