package nl.bioinf.webbased1.model;

import java.util.ArrayList;

public class PatternMatch {
    public static ArrayList patternMatcher(ArrayList proteins, String input) {
        ArrayList<Protein> matched_proteins = new ArrayList<>();
        for (int i = 1; i < proteins.size(); i++) {
            Protein protein = (Protein) proteins.get(i);
            if (input.matches(".*" + protein.getPa() + ".*")) {
                matched_proteins.add(protein);
                //System.out.println("--MATCHED--");
                //System.out.println("INPUT STRING: " + input);
                //System.out.println("PATTERN: " + protein.getPa());
            }
        }
        return matched_proteins;
    }
}
