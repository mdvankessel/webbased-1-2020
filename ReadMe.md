# Webbased 1 2020
------

## About
**AUTHOR:** Maarten van Kessel

**Student Number:** 356200

**PROJECT:** 4. Prosite Scanner

**YEAR:** 2020

## Project Description

Prosite (https://prosite.expasy.org/) is a tool that can be used to perform pattern searches on protein sequences. The download section (ftp://ftp.expasy.org/databases/prosite) holds the file `prosite.dat` that contains all known Prosite patterns. The file ` profile.txt ` describes the Prosite pattern syntax.
For this project you have to create a website that can be used to scan proteins uploaded by your website users for known Prosite patters. The web app should load all Prosite patterns at startup (as regular expression patterns of course). After the scan, you should report all pattern occurrences in a user-friendly way.
A history listing of this analysis should be offered to your users: the last 5 analyses should be available for review.